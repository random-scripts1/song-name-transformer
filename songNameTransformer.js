const mm = require("music-metadata");
const fs = require("fs");
const util = require("util");
const path = require("path");

// const folderPath = "C:\\Users\\chris\\Music\\iTunes\\iTunes Media\\Voice Memos";
const FOLDER_PATH = ".\\songs";
const FILE_FORMAT = '.m4a'

async function getAllFiles(path) {
  const dirents = [];
  const dir = await fs.promises.opendir(path);
  for await (const dirent of dir) {
    dirents.push(dirent);
  }
  return dirents;
}

async function runProgram() {
  const dirents = await getAllFiles(FOLDER_PATH);
  for (const dirent of dirents) {
    console.log(dirent);
    const direntPath = path.resolve(FOLDER_PATH, dirent.name);
    mm.parseFile(direntPath)
      .then((metadata) => {
        // console.log(util.inspect(metadata, { showHidden: false, depth: null }));
        const prop = metadata.native.iTunes[0];
        if (prop.id === "©nam") {
          const newPath = path.resolve(FOLDER_PATH, prop.value+FILE_FORMAT);
          fs.rename(direntPath, newPath, function (err) {
            if (err) {
              console.log(err);
            } else {
              console.log(`Successfully renamed the file from ${direntPath} to ${newPath}`);
            }
          });
        }
      })
      .catch((err) => {
        console.error(err.message);
      });
  }
}

runProgram();
