# Song Name Transformer
Transforms a music file (m4a has been tested) to a new name from the songs metadata attribute "title"

### Problem
When trying to store my Iphone voice memos in a cloud storage I use, each voice memo had as the file name a long sequence of numbers. The title I gave them was instead stored in the metadata of each m4a file. So with the hundred voice memos I have, I made a script to automatically read each title in the metadata and rename the file accordingly.